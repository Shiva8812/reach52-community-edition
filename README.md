# README #

## reach52 Community edition
reach52 Community edition is an open-source Node.js codebase aiming to support payments, mWallets, and other Healthcare e-commerce functionalities with an aim to grow and let others grow by laying a strong foundation.

### What is this repository for? ###

* Creating a plug-n-play solution for the existing/upcoming Healthcare e-commerce platforms
* Current version of this repository is version 0.0.1 (v0.0.1)

## Motivation
reach52 is at the forefront of the HealthCare e-commerce domain in catering to the under-privileged health and to get them the much-needed healthcare facilities. HealthCare is not a luxure, it is the right of every human and thus we are helping them get wht they rightly deseeve.

## Build status
Build status of continus integration - reach52 Community edition 

[![Build Status](CI/CD Repo link: TBD](CI/CD Link: TBD)

## Tech/framework used
Node.js is a cross-platform runtime library and environment for running JavaScript applications outside the browser. This is a free and open source tool used for creating server-side JS applications.
Node.js applications are written in JavaScript. This application runs within the Node.js runtime on Linux and Microsoft Windows. This framework offers a rich library of various JavaScript modules to simplify web development process.

More details on Node.js: https://nodejs.dev/

For begineers on Node.js, checkout the [How do I start with Node.js after I installed it?](https://nodejs.org/en/docs/guides/getting-started-guide/)

## Contributing Guidelines

Please refer to the [CONTRIBUTING.MD](https://bitbucket.org/reach52/reach52-community-edition/src/master/CONTRIBUTING.md) file for more details on the coding standard and other measures to take.


## Features
* mWallets
* Generic Payment gateway
* Supplier Onboarding
* Discounts/Points logics

## Installation/Deployment
This is an API open source project built in Node.js and hence this project needs to be deployed on to a server.

You can take a look at the API documentation here: [API Documentation](https://api-open.reach52.com/api-docs/)

There are many ways of hosting the API but for the sake of simplicity, lets take the case of hosting this in AWS from where your endpoints are to be exposed. To do this, please follow the steps mentioned [here](https://aws.amazon.com/getting-started/hands-on/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/module-4/).

***Once the hosting is done, you can test the end-points using any REST client like Postman***
## API's description
1.API Name- /api/upload/medications
MEDICATION DETAILS EXCELSHEET IMPORT
It uploads the csv and excel sheet format
2.API Name- /api/view/files
THIS API IS TO SHOW ALL CATALOUGUE FILES DETAILS
3.API Name- /api/view/fileprocess
THIS API IS TO PROCESS FAILUER UPLOAD FILE & RE UPLOAD THESE FILES 
4.Order Points Create
API Name- /api/processorder
5.API Name- /api/pointconversion
THIS API WILL GIVE CURRENCY DETAILS OF GIVEN COUNTRY CODE
We have 3 countryCode
*India - IND
*Combodia - KHM
*Philipines - PHL
6.API Name- /api/pointsexpire
THIS API IS TO DEACTIVATE/LAPSE ALL POINTSAUDIT COLLECTION DATA WHOSE EXPIRY DATE IS SAME DATE
7.API Name- /api/user/transactiondetails
THIS API TO GET THE USER LATEST TRANSACTION DETAILS
8.API Name- /api/redeemdetails
THIS API TO GET THE USER POINTS REDEEMPTION
This API for to get the user Point Redeemption 
9.API Name- /api/add/supplier
IT CREATES THE NEW SUPPLIER DETAILS
10.API Name- /api/view/singlesupplier
THIS API WILL GIVE A SINGLE SUPPLIER DETAILS AS PER SUPPLIERCODE
11.API Nmae- /api/view/allsuppliers
THIS API WILL GIVE ALL SUPPLIERS DETAILS
## DataBase
For OrderMedicine & ResidentUser use the collection object which is created in root folder named as sampleCollection.
rest of collections you can use your own mongo database connections

## .env file
PORT=8000
SUCCESSDIR='Catalouge_Import/'
FAILUERDIR='Failure_Catalogue/'
NODE_ENV=development
TIMEZONE='Asia/Kolkata'
DEV_URL=''
PROD_URL=''

## Tests
Stpes on how to run the tests will be added once the tests are ready in the repo.
1.Run command npm test
2.it will test all apis & throw errors from apis as respected to api functionality
## How to use?
Steps will be added here
1.Set up database url in .env file 
2.run npm start , it will show connected port & database connection status & database name
3.We need ResidentUser & OrderMedicine collection in Mongo DB as per schema

### Who do I talk to? ###

* Repo owner or admin - [Lakshmi Narasimhan](lakshmi@reach52.com)

### Who is reach52? ###

To know us and our social impact cause better, visit us at [reach52](https://reach52.com/) 

## License

MIT License.

Copyright (c) 2021 reach52 Community edition.